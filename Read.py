#!/usr/bin/env python

import RPi.GPIO as GPIO
import SimpleMFRC522
import sys

GPIO.setwarnings(False) # silence warnings about already in INPUT mode

reader = SimpleMFRC522.SimpleMFRC522()

id, text = reader.read()
print(id)
GPIO.cleanup()
