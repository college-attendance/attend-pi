import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
`

const ResultsBody = styled.div`
  flex: 1;
  display: flex;
  background: ${(p) =>
    p.inside ? 'hsla(89, 73%, 75%, 1)' : 'hsl(212, 72%, 75%)'};
`

const Message = styled.div`
  font-size: 3em;
  font-weight: bold;
  margin: auto;
  text-align: center;
`

const ScanResults = ({ data, entering, onCancel }) => {
  const name = typeof data.name === 'undefined' ? data.id : data.name
  const welcomeMessage = (
    <Message>
      Welcome, <br />
      {name}!
    </Message>
  )
  const goodbyeMessage = (
    <Message>
      Thanks for coming, <br />
      {name}.
    </Message>
  )
  const message = entering ? welcomeMessage : goodbyeMessage

  return (
    <Container onClick={onCancel}>
      <ResultsBody inside={entering}>{message}</ResultsBody>
    </Container>
  )
}

export default ScanResults
