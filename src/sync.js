import { pathExists, readFile, writeFile } from 'fs-extra'
import path from 'path'
import uuid from 'uuid/v4'
import delay from 'delay'
import timeout from 'p-timeout'

import { getScansSince } from './db'

let lastSync
export const startSync = async (socket) => {
  while (true) {
    lastSync = Date.now()
    await timeout(doSync(socket), 5000, () => {
      console.log('Sync timeout reached.  Trying again.')
    })

    const nextSync = lastSync + 30000
    await delay(Math.max(0, nextSync - Date.now()))
  }
}

const doSync = async (socket) => {
  console.log('starting sync probe')
  const clientId = await getClientId()
  console.log('clientId =', clientId)
  const lastSync = await syncRequest(socket, clientId)
  console.log('lastSync was', Date.now() - lastSync, 'ms ago', lastSync)

  console.log('starting sync')
  let scans = await getScansSince(lastSync - 10000)
  console.log(`syncing ${scans.length} entries`)
  await syncScans(socket, clientId, scans)
}

const cidPath = path.join(__dirname, '.client_id')
const getClientId = async () => {
  const exists = await pathExists(cidPath)
  if (!exists) {
    const id = uuid()
    await writeFile(cidPath, id)
  }

  return await readFile(cidPath, { encoding: 'utf8' })
}

const syncRequest = (socket, cid) => {
  return new Promise((resolve) => {
    socket.emit('sync-request', cid, (clientId) => resolve(clientId))
  })
}

const syncScans = async (socket, cid, scans) => {
  socket.emit('sync', cid, scans)
}
