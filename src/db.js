import Redis from 'ioredis'
import level from 'level'

const redis = new Redis()
const db = level('~/attend.db')

export const studentFromId = async (id) => {
  const result = await redis.get(`student:${id}`)
  return JSON.parse(result)
}

export const lastScan = async (id) => {
  const results = await redis.zrange(`history:${id}`, -1, -1)

  if (results.length === 0) {
    return null
  }

  const entry = JSON.parse(results[0])
  return entry
}

export const makeScan = (entering, id) => {
  const now = Date.now()
  return { entering, date: now, id }
}

export const writeScan = async (scan) => {
  await redis.zadd(
    `history:${scan.id}`,
    scan.date,
    JSON.stringify(scan, null, 2),
  )
}

export const getScansSince = (date) => {
  return new Promise((resolve) => {
    const scans = []

    const keyStream = redis.scanStream({
      match: 'history:*',
    })

    keyStream.on('data', (dbKeys) => {
      keyStream.pause()

      dbKeys.forEach((dbKey) => {
        const historyStream = redis.zscanStream(dbKey, {
          match: '*',
        })

        historyStream.on('data', (historyEntries) => {
          let deleteNext = true
          historyEntries.forEach((historyEntry) => {
            // this function is called with alternating values and scores
            // we only want the value
            deleteNext = !deleteNext
            if (deleteNext) {
              return
            }

            // Only sync data after the date
            const entryData = JSON.parse(historyEntry)
            if (entryData.date >= date) {
              scans.push(historyEntry)
            }
          })
        })

        historyStream.on('end', () => {
          keyStream.resume()
        })
      })
    })

    keyStream.on('end', () => {
      resolve(scans)
    })
  })
}

exports.writeScans = async (scans) => {
  const pipeline = redis.pipeline()
  scans.forEach((scan) => {
    const scanData = JSON.parse(scan)
    console.log('scanData', scanData)
    pipeline.zadd(`history:${scanData.id}`, scanData.date, scan, (err) => {
      if (err) {
        console.log(err)
        // throw new Error('Error sending command pipeline')
      }
    })
  })
  await pipeline.exec()
}

export const batchSaveUsers = async (users) => {
  const pipeline = redis.pipeline()
  users.forEach((user) => {
    const userString = JSON.stringify(user, null, 2)
    pipeline.set(`users:${user.personId}`, userString, (err) => {
      if (err) {
        console.log(err)
      }
    })
  })
  await pipeline.exec()
}

export const rebuildScanIndex = async () => {
  // clear the deletion list
  // add all elements to the deletion list
  // iterate over users, adding/updating index entries
  // and removing their id from the deletion list
  // delete everything in the list
  //
  // a deletion list is probably faster than a keep list
  // because a change list requires a client-side lookup of
  // every key to check if it's a keeper
}

export const getAll = async () => {
  // scan stream keys from the db and collect into an array
}

export const getLastPopuliUsersSync = async () => {
  return await Number(redis.get('state:last_populi_users_sync'))
}

export const setLastPopuliUsersSync = async (lastSync) => {
  return await redis.set('state:last_populi_users_sync', lastSync)
}
