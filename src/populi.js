import xml2js from 'xml2js'
import pMap from 'p-map'
import { getLastPopuliUsersSync, batchSaveUsers } from './db'

export const startSync = (config, interval) => {
  setTimeout(async () => {
    await runSync(config)
    startSync(config)
  }, interval)
}

export const runSync = async (config) => {
  const lastSync = await getLastPopuliUsersSync()
  if (!lastSync) {
    await syncAllUsers(config)
  } else {
    await syncUsersSince(config, lastSync)
  }
}

// get list of students
const config = {
  endpoint: 'https://weimar.populiweb.com/api/',
  accessKey:
    '6c4915291f3a5c5ca6a666d394c926677269ed632e55dc1da5a334264f52af074a3c8ac2614e7b206171df45d4f22814edf4a51db8eeff3fa9641ba2ce5c270e3e72a12c918e3beffcc7b5d64c3dae89a1959d680a76a1feba8dea97dac33d3d2b204b662818cce17746308f9edd4c8c5c27c69732965d4a68b21caf7fc45a0ff90839259409ee94e70ba238d44d9b1d6304b501380e9aee0dd9e4c869a5249dbade09987d51',
}

export const syncAllUsers = async (config) => {
  const getUsersResult = await callPopuli(config, { task: 'getUsers' })

  // console.log(getUsersResult.response.person)

  const users = await pMap(
    getUsersResult.response.person,
    async (user) => {
      const newUser = {
        firstName: user.first[0],
        lastName: user.last[0],
        personId: user.person_id[0],
        username: user.username[0],
        badgeId: '',
      }

      const getCustomFieldsResult = await callPopuli(config, {
        task: 'getCustomFields',
        person_id: newUser.personId,
      })

      const customFields = getCustomFieldsResult.response.custom_field

      if (Array.isArray(customFields)) {
        const badgeId = customFields.find((f) => f.name[0] === 'Badge ID')
        if (badgeId) {
          newUser.badgeId = badgeId
        }
      }

      return newUser
    },
    { concurrency: 10 },
  )

  await batchSaveUsers(users)
  await setLastPopuliUsersSync(Date.now())

  return users
}

export const syncUsersSince = async (config, since) => {
  // call populi for changelist
  // format changelist
  // hand over to DB as a batch operation
  // rebuild scan index
}

export const callPopuli = async ({ accessKey, endpoint }, params) => {
  const formData = new FormData()

  // append API key
  formData.append('access_key', accessKey)

  // append custom params
  Object.keys(params).forEach((key) => {
    formData.append(key, params[key])
  })

  const response = await fetch(endpoint, {
    method: 'POST',
    body: formData,
  })

  const body = await response.text()

  return await parseXmlString(body)
}

const parseXmlString = (xmlString) => {
  return new Promise((resolve, reject) => {
    xml2js.parseString(xmlString, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}
