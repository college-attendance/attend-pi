import '@babel/polyfill'

import ReactDOM from 'react-dom'
import React from 'react'
import { injectGlobal } from 'styled-components'

import App from './App'

injectGlobal`
  html {
    font-family: 'Open Sans', sans-serif;
  }
`

const domContainer = document.querySelector('#root')
ReactDOM.render(<App />, domContainer)
