import React from 'react'
import styled from 'styled-components'
import io from 'socket.io-client'

import ScanResults from './ScanResults'
import { scanId } from './scanner'
import { studentFromId, lastScan, writeScan, makeScan } from './db'
import { startSync } from './sync'
import * as populi from './populi'
import cosmiconfig from 'cosmiconfig'

const explorer = cosmiconfig('attend')

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: fixed;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
`

const Welcome = styled.h1`
  margin: auto;
  text-align: center;
`

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      scan: null,
    }
  }
  async componentDidMount() {
    // load configuration

    const config = await explorer.search()

    // connect to main server
    // this.socket = io('http://srv-attendance.campus.weimar.org:3400/')

    // start periodic sync
    // setTimeout(() => startSync(this.socket), 0)

    populi.startSync(config, 10000)

    // scanner event loop
    this.runScanner()
  }
  runScanner = () => {
    console.log('Re-running scanner')
    setTimeout(() => {
      this.readScan()
        .catch((e) => {
          console.log('scan error', e)
        })
        .then(() => {
          this.runScanner()
        })
    }, 0)
  }
  readScan = async () => {
    const id = await scanId()
    // is this the already displayed scan?
    // (preliminary check to avoid thrashing the database)
    if (!this.state.scan || this.state.scan.id !== id) {
      // duplicate scan.  Restart the counter.
      this.restartTimeout()
      // look up the name
      const { name } = await studentFromId(id)

      // have they scanned recently?
      const last = (await lastScan(id)) || { date: 0, entring: false }
      let entering

      if (last.date + 45000 < Date.now()) {
        console.log('no scan within 45s', last.entering)
        // No scan within 30 seconds.  Record a change
        entering = !last.entering

        const scan = makeScan(entering, id)
        console.log(scan)
        // this.socket.emit('scan', scan)
        await writeScan(scan)
      } else {
        console.log('recently scanned', last.entering)
        // Recently scanned.  Just display same value.
        entering = last.entering
      }

      // push to app state
      this.setState({ scan: { id, name, entering } })
    }
  }
  restartTimeout = () => {
    this.clearTimeout()
    this.resetTimer = window.setTimeout(this.resetScan, 8000)
  }
  clearTimeout = () => {
    if (typeof this.resetTimer !== 'undefined') {
      window.clearTimeout(this.resetTimer)
    }
  }
  resetScan = () => {
    clearTimeout()

    this.setState({ scan: null })
  }
  render() {
    let contents = <Welcome>Please scan your student ID.</Welcome>
    if (this.state.scan) {
      contents = (
        <ScanResults
          data={this.state.scan}
          entering={this.state.scan.entering}
          onCancel={this.resetScan}
        />
      )
    }
    return <Container>{contents}</Container>
  }
}

export default App
