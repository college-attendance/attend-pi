import execa from 'execa'
import path from 'path'

export const scanId = async () => {
  while (true) {
    const { stdout } = await execa('/usr/bin/python', [
      path.join(__dirname, '../Read.py'),
    ])

    console.log('raw scan:', stdout)

    const number = Number.parseInt(stdout)
    if (number && String(number).length === 12) {
      return number
    }
  }
}
